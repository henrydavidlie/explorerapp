//
//  ApiService.swift
//  ExplorerApp
//
//  Created by Henry David Lie on 05/05/21.
//

import Foundation

class ApiService {
    
    let baseURL = "http://192.168.1.88/ExplorerServer/public"
    
    func getExplorerPrev(completion: @escaping ([ExplorerPrev]) -> Void) {
        guard let url = URL(string: "\(baseURL)/api/explorer-preview") else { return }
        
        URLSession.shared.dataTask(with: makeRequest(url: url)) { data, response, error in
            if error != nil {
                print("\(error?.localizedDescription ?? "")")
            } else if let data = data, let response = response as? HTTPURLResponse, response.statusCode == 200 {
                do {
                    print("prev: \(data)")
                    let explorerPrev = try JSONDecoder().decode(ExplorerPrevResponse.self, from: data)
                    DispatchQueue.main.async {
                        completion(explorerPrev.data)
                    }
                } catch {
                    print(error)
                }
            }
        }.resume()
    }
    
    func getExplorer(sectionId: Int, completion: @escaping ([Explorer]) -> Void) {
        guard let url = URL(string: "\(baseURL)/api/explorers/\(sectionId)") else { return }
        
        URLSession.shared.dataTask(with: makeRequest(url: url)) { data, response, error in
            if error != nil {
                print("\(error?.localizedDescription ?? "")")
            } else if let data = data, let response = response as? HTTPURLResponse, response.statusCode == 200 {
                do {
                    print("expSec: \(data)")
                    let exResponse = try JSONDecoder().decode(ExplorerResponse.self, from: data)
                    DispatchQueue.main.async {
                        completion(exResponse.explorers)
                    }
                } catch {
                    print(error)
                }
            }
        }.resume()
    }
    
    func getProfile(completion: @escaping (Explorer) -> Void) {
        guard let url = URL(string: "\(baseURL)/api/profile") else { return }
        
        URLSession.shared.dataTask(with: makeRequest(url: url, requireAuth: true)) { data, response, error in
            if error != nil {
                print("\(error?.localizedDescription ?? "")")
            } else if let data = data, let response = response as? HTTPURLResponse, response.statusCode == 200 {
                do {
                    print("profile: \(data)")
                    let resp = try JSONDecoder().decode(Explorer.self, from: data)
                    DispatchQueue.main.async {
                        completion(resp)
                    }
                } catch {
                    print(error)
                }
            }
        }.resume()
    }
    
    func updateBio(completion: @escaping (String) -> Void) {
        guard let url = URL(string: "\(baseURL)/api/update-profile") else { return }
    }
    
    func funcLogin(param: [String: String], completion: @escaping (String) -> Void) {
        guard let url = URL(string: "\(baseURL)/api/login") else { return }
        
        URLSession.shared.dataTask(with: makeRequest(url: url, method: "POST", param: param)) { data, response, error in
            if error != nil {
                print("\(error?.localizedDescription ?? "")")
            } else if let data = data, let response = response as? HTTPURLResponse, response.statusCode == 200 {
                do {
                    print("login: \(data)")
                    let resp = try JSONDecoder().decode(UserResponse.self, from: data)
                    DispatchQueue.main.async {
                        UserDefaults.standard.set(resp.token, forKey: "AccessToken")
                        completion(resp.message)
                    }
                } catch {
                    print(error)
                }
            }
        }.resume()
    }
    
    func funcCheckEmail(param: [String: String], completion: @escaping (String) -> Void) {
        guard let url = URL(string: "\(baseURL)/api/check-email") else { return }
        
        URLSession.shared.dataTask(with: makeRequest(url: url, method: "POST", param: param)) { data, response, error in
            if error != nil {
                print("\(error?.localizedDescription ?? "")")
            } else if let data = data, let response = response as? HTTPURLResponse, response.statusCode == 200 {
                do {
                    print("checkMail: \(data)")
                    let resp = try JSONDecoder().decode(MessageResponse.self, from: data)
                    DispatchQueue.main.async {
                        completion(resp.message)
                    }
                } catch {
                    print(error)
                }
            }
        }.resume()
    }
    
    func funcSendOtp(param: [String: String], completion: @escaping (String) -> Void) {
        guard let url = URL(string: "\(baseURL)/api/check-otp") else { return }
        
        URLSession.shared.dataTask(with: makeRequest(url: url, method: "POST", param: param)) { data, response, error in
            if error != nil {
                print("\(error?.localizedDescription ?? "")")
            } else if let data = data, let response = response as? HTTPURLResponse, response.statusCode == 200 {
                do {
                    print("checkOTP: \(data)")
                    let resp = try JSONDecoder().decode(MessageResponse.self, from: data)
                    DispatchQueue.main.async {
                        completion(resp.message)
                    }
                } catch {
                    print(error)
                }
            }
        }.resume()
    }
    
    func funcLogout(completion: @escaping (String) -> Void) {
        guard let url = URL(string: "\(baseURL)/api/logout") else { return }
        
        URLSession.shared.dataTask(
            with: makeRequest(url: url, method: "POST", requireAuth: true)
        ) { data, response, error in
            if error != nil {
                print("\(error?.localizedDescription ?? "")")
            } else if let data = data, let response = response as? HTTPURLResponse, response.statusCode == 200 {
                do {
                    print("logout: \(data)")
                    let resp = try JSONDecoder().decode(MessageResponse.self, from: data)
                    DispatchQueue.main.async {
                        let domain = Bundle.main.bundleIdentifier!
                        UserDefaults.standard.removePersistentDomain(forName: domain)
                        completion(resp.message)
                    }
                } catch {
                    print(error)
                }
            }
        }.resume()
    }
    
    func funcChangePassword(completion: @escaping (String) -> Void) {
        guard let url = URL(string: "\(baseURL)/api/change-password") else { return }
    }
    
    func makeRequest(
        url: URL,
        method: String = "GET",
        requireAuth: Bool = false,
        param: [String: Any] = [:]
    ) -> URLRequest {
        let token = UserDefaults.standard.string(forKey: "AccessToken")
        var request = URLRequest(url: url)
        if !param.isEmpty {
            request.httpBody = try? JSONSerialization.data(withJSONObject: param)
            request.setValue("application/json", forHTTPHeaderField: "Content-Type")
        }
        request.httpMethod = method
        request.setValue("application/json", forHTTPHeaderField: "Accept")
        if requireAuth {
            request.setValue(token, forHTTPHeaderField: "Authorization")
        }
        print(request)
        return request
    }
}
