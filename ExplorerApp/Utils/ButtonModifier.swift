//
//  ButtonModifier.swift
//  ExplorerApp
//
//  Created by Henry David Lie on 07/05/21.
//

import SwiftUI

struct ButtonModifier: ViewModifier {
    
    func body(content: Content) -> some View {
        return content
            .frame(width: UIScreen.main.bounds.width - 50, height: 40)
            .background(Color(UIColor.systemOrange))
            .cornerRadius(20)
            .padding(.bottom, 10)
    }
}
