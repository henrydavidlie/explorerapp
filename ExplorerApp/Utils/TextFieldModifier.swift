//
//  TextFieldModifier.swift
//  ExplorerApp
//
//  Created by Henry David Lie on 07/05/21.
//

import SwiftUI

struct TextFieldModifier: ViewModifier {
    
    func body(content: Content) -> some View {
        return content
            .padding()
            .autocapitalization(.none)
            .disableAutocorrection(true)
            .frame(width: 345, height: 42)
            .cornerRadius(10)
            .border(Color(UIColor.separator))
    }
}
