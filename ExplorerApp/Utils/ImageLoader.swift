//
//  ImageLoader.swift
//  ExplorerApp
//
//  Created by Henry David Lie on 06/05/21.
//

import SwiftUI
import SDWebImageSwiftUI

struct ImageLoader: View {
    
    @State var imageUrl: String
    @State var detail: Bool = false
    
    var body: some View {
        WebImage(url: URL(string: imageUrl))
            .resizable()
            .indicator(Indicator.progress)
            .transition(.fade(duration: 0.8))
            .scaledToFill()
            .frame(width: detail ? 135 : 50, height: detail ? 135 : 50)
            .cornerRadius(10)
    }
}

struct ImageLoader_Previews: PreviewProvider {
    static var previews: some View {
        ImageLoader(imageUrl: "")
    }
}
