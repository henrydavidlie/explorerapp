//
//  ExplorerAppApp.swift
//  ExplorerApp
//
//  Created by Henry David Lie on 29/04/21.
//

import SwiftUI

@main
struct ExplorerAppApp: App {
    var body: some Scene {
        WindowGroup {
            BridgeView()
        }
    }
}
