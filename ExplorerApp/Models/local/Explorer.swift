//
//  Explorer.swift
//  ExplorerApp
//
//  Created by Henry David Lie on 30/04/21.
//

import Foundation

struct Explorer: Codable {
    var id: Int
    var name: String
    var email: String?
    var photoURL: String
    var bio: String?
    var expertise: String
    var teams: [String]?
    var shift: String

    static let example = Explorer(
        id: 1,
        name: "Henry David Lie",
        email: "henrydavidlie@gmail.com",
        photoURL: "https://dl.airtable.com/.attachments/a21a30ff756f18d90fe820e6ce1f8473/842d271a/HenryDavid.jpg",
        expertise: "Tech / IT / IS",
        teams: ["Team 4/ Money Gang"],
        shift: "Afternoon")
}
