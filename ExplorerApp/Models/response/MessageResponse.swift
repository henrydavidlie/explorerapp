//
//  MessageResponse.swift
//  ExplorerApp
//
//  Created by Henry David Lie on 06/05/21.
//

import Foundation

struct MessageResponse: Codable {
    var message: String
}
