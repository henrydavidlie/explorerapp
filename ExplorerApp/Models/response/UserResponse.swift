//
//  UserResponse.swift
//  ExplorerApp
//
//  Created by Henry David Lie on 06/05/21.
//

import Foundation

struct UserResponse: Codable {
    var message: String
    var user: Explorer
    var token: String
}
