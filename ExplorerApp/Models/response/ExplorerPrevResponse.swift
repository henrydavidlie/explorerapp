//
//  ExplorerPrevResponse.swift
//  ExplorerApp
//
//  Created by Henry David Lie on 06/05/21.
//

import Foundation

struct ExplorerPrevResponse: Codable {
    var data: [ExplorerPrev]
}

struct ExplorerPrev: Codable {
    var id: Int
    var section: String
    var items: [Explorer]
}
