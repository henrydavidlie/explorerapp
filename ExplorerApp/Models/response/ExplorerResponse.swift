//
//  ExplorerResponse.swift
//  ExplorerApp
//
//  Created by Henry David Lie on 30/04/21.
//

import Foundation

struct ExplorerResponse: Codable {
    let explorers: [Explorer]
    
    enum CodingKeys: String, CodingKey {
        case explorers = "data"
    }
}
