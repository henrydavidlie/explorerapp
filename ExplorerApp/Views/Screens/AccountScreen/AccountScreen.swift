//
//  AccountScreen.swift
//  ExplorerApp
//
//  Created by Henry David Lie on 30/04/21.
//

import SwiftUI

struct AccountScreen: View {
    
    @State private var showBioModal = false
    @State private var showSettingModal = false
    @State var isLoggedOut = false
    @State var user: Explorer = Explorer.example
    
    var body: some View {
        NavigationView {
            ZStack {
                Color("Background").ignoresSafeArea()
                ScrollView {
                    // MARK: Image Profile
                    VStack {
                        ImageLoader(imageUrl: user.photoURL, detail: true)
                            .clipShape(Circle())
                        Text(user.name)
                            .font(.headline)
                            .padding(.top)
                        Text(user.expertise)
                            .font(.subheadline)
                            .padding(.bottom)
                    }
                    .padding(.top)
                    
                    // MARK: Bio
                    Button(action: {
                        showBioModal.toggle()
                    }, label: {
                        VStack {
                            Text(user.bio ?? "Describe yourself")
                                .foregroundColor(.black)
                                .padding()
                                .frame(width: UIScreen.main.bounds.width - 28, height: 90, alignment: .leading)
                                .background(Color(.white))
                                .cornerRadius(15)
                        }
                    })
                    .sheet(isPresented: $showBioModal) {
                        BioModal()
                    }
                }
                .onAppear {
                    ApiService().getProfile { (explorer) in
                        self.user = explorer
                    }
                }
                .navigationTitle("Account")
                .navigationBarItems(
                    trailing:
                        Button(action: {
                            showSettingModal.toggle()
                        }, label: {
                            Image(systemName: "gear")
                                .foregroundColor(.orange)
                        })
                        .sheet(isPresented: $showSettingModal) {
                            SettingsModal(isShowing: self.$showSettingModal, isLogout: self.$isLoggedOut)
                        }
                )
                .fullScreenCover(isPresented: $isLoggedOut, content: {
                    BridgeView()
                })
            }
        }
    }
}

#if DEBUG
struct AccountScreen_Previews: PreviewProvider {
    static var previews: some View {
        AccountScreen(user: Explorer.example)
    }
}
#endif
