//
//  SectionScreen.swift
//  ExplorerApp
//
//  Created by Henry David Lie on 06/05/21.
//

import SwiftUI

struct SectionScreen: View {
    
    @State var explorers: [Explorer] = []
    @State var sectionName: String
    @State var sectionId: Int
    
    var body: some View {
        List {
            ForEach(explorers, id: \.id) { explorer in
                NavigationLink(
                    destination: DetailScreen(learner: explorer),
                    label: {
                        LearnerCardView(learner: explorer)
                    })
            }
        }.onAppear {
            ApiService().getExplorer(sectionId: sectionId, completion: { (explorers) in
                self.explorers = explorers
            })
        }
        .listStyle(GroupedListStyle())
        .navigationTitle(sectionName)
        .navigationBarTitleDisplayMode(.inline)
    }
}

struct SectionScreen_Previews: PreviewProvider {
    static var previews: some View {
        SectionScreen(sectionName: "Afternoon Learners", sectionId: 1)
    }
}
