//
//  DetailScreen.swift
//  ExplorerApp
//
//  Created by Henry David Lie on 06/05/21.
//

import SwiftUI

struct DetailScreen: View {
    @State var learner: Explorer
    @State private var showBioModal = false
    
    var body: some View {
        ZStack {
            Color("Background").ignoresSafeArea()
            ScrollView {
                VStack {
                    // MARK: Image Profile
                    VStack {
                        ImageLoader(imageUrl: learner.photoURL, detail: true)
                            .clipShape(Circle())
                        Text(learner.name)
                            .font(.headline)
                            .padding(.top)
                        Text(learner.expertise)
                            .font(.subheadline)
                            .padding(.bottom)
                    }
                    .padding(.top)
                    
                    // MARK: Bio
                    Button(action: {
                        showBioModal.toggle()
                    }, label: {
                        VStack {
                            Text(learner.bio ?? "No description")
                                .foregroundColor(.black)
                                .padding()
                                .frame(width: UIScreen.main.bounds.width - 28, height: 90, alignment: .leading)
                                .background(Color(.white))
                                .cornerRadius(15)
                        }
                    })
                    .sheet(isPresented: $showBioModal) {
                        BioModal()
                    }
                }
            }
            .navigationTitle(learner.name)
            .navigationBarTitleDisplayMode(.inline)
        }
    }
}

#if DEBUG
struct DetailScreen_Previews: PreviewProvider {
    static var previews: some View {
        DetailScreen(learner: Explorer.example)
    }
}
#endif
