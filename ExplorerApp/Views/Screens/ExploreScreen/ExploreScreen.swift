//
//  ExploreScreen.swift
//  ExplorerApp
//
//  Created by Henry David Lie on 30/04/21.
//

import SwiftUI

struct ExploreScreen: View {
    @State var preview: [ExplorerPrev] = []
    
    var body: some View {
        NavigationView {
            List {
                ForEach(preview, id: \.id) { section in
                    Section(header: HStack {
                        Text(section.section)
                        Spacer()
                        NavigationLink(
                            destination:
                                SectionScreen(sectionName: section.section,
                                              sectionId: section.id),
                            label: {
                                Text("See all")
                            })
                    }
                    ) {
                        ForEach(section.items, id: \.id) { item in
                            NavigationLink(
                                destination: DetailScreen(learner: item),
                                label: {
                                    LearnerCardView(learner: item)
                                })
                        }
                    }
                }
            }.onAppear {
                ApiService().getExplorerPrev { (preview) in
                    self.preview = preview
                }
            }
            .navigationTitle("Explore")
            .listStyle(GroupedListStyle())
        }.accentColor(Color(UIColor.systemOrange))
    }
}

#if DEBUG
struct ExploreScreen_Previews: PreviewProvider {
    static var previews: some View {
        ExploreScreen()
    }
}
#endif
