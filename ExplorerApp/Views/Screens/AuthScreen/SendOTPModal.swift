//
//  SendOTPModal.swift
//  ExplorerApp
//
//  Created by Henry David Lie on 06/05/21.
//

import SwiftUI

struct SendOTPModal: View {
    @Environment(\.presentationMode) private var presentationMode
    @Binding var showSendOTPModal: Bool
    @Binding var showLoginModal: Bool
    @State var message: String = ""
    @Binding var email: String
    @State private var otp: String = ""
    
    var body: some View {
        NavigationView {
            ZStack {
                Color("Background").ignoresSafeArea()
                VStack {
                    Text("Enter OTP you\nreceived on your email")
                        .font(.title2)
                        .multilineTextAlignment(.center)
                        .padding(.top)
                        .padding(.bottom)
                    TextField(
                        "Enter OTP",
                        text: $otp)
                        .modifier(TextFieldModifier())
                        .multilineTextAlignment(.center)
                    Spacer()
                    Button(action: {
                        ApiService().funcSendOtp(param: ["email": self.email, "otp": otp], completion: { (message) in
                            self.message = message
                            if message == "success" {
                                self.showLoginModal.toggle()
                                self.showSendOTPModal.toggle()
                            } else {
                                self.showSendOTPModal.toggle()
                            }
                        })
                    }, label: {
                        Text("Verify")
                            .foregroundColor(.white)
                    })
                    .modifier(ButtonModifier())
                    .disabled(otp.isEmpty)
                    CustomSpacer()
                }
            }
            .navigationBarItems(
                leading:
                    Button("Close") {
                        presentationMode.wrappedValue.dismiss()
                    }.accentColor(.orange)
            )
            .navigationBarTitleDisplayMode(.inline)
        }
    }
}
