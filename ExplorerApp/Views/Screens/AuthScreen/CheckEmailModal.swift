//
//  CheckEmailModal.swift
//  ExplorerApp
//
//  Created by Henry David Lie on 06/05/21.
//

import SwiftUI

struct CheckEmailModal: View {
    
    @Binding var showSendOTPModal: Bool
    @Binding var isWithEmail: Bool
    @Binding var showLoginModal: Bool
    @State var message: String = ""
    @State private var email: String = ""
    @Binding var mail: String
    
    var body: some View {
        NavigationView {
            ZStack {
                Color("Background").ignoresSafeArea()
                VStack {
                    Text("Enter email you used for\nDeveloper Academy")
                        .font(.title2)
                        .multilineTextAlignment(.center)
                        .padding(.top)
                        .padding(.bottom)
                    TextField(
                        "Enter email",
                        text: $email)
                        .modifier(TextFieldModifier())
                    Spacer()
                    Button(action: {
                        ApiService().funcCheckEmail(param: ["email": email], completion: { (message) in
                            self.message = message
                            if message != "next" {
                                self.mail = email
                                self.showSendOTPModal.toggle()
                                self.isWithEmail.toggle()
                            } else {
                                self.showLoginModal.toggle()
                                self.isWithEmail.toggle()
                            }
                        })
                    }, label: {
                        Text("Check email")
                            .foregroundColor(.white)
                    })
                    .modifier(ButtonModifier())
                    .disabled(email.isEmpty)
                    CustomSpacer()
                }
            }
            .navigationBarItems(
                leading:
                    Button("Close") {
                        self.isWithEmail = false
                    }.accentColor(.orange)
            )
            .navigationBarTitleDisplayMode(.inline)
        }
    }
}
