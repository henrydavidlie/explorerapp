//
//  GreetingScreen.swift
//  ExplorerApp
//
//  Created by Henry David Lie on 07/05/21.
//

import SwiftUI

struct GreetingScreen: View {
    
    @State private var showMain = false
    
    var body: some View {
        VStack {
            Spacer()
            Button(action: {
                withAnimation {
                    self.showMain.toggle()
                }
            }, label: {
                Text("Start exploring")
                    .foregroundColor(.white)
            })
            .modifier(ButtonModifier())
            .fullScreenCover(isPresented: $showMain, content: {
                MainView()
            })
            Rectangle()
                .foregroundColor(Color(UIColor.systemBackground))
                .frame(height: 100)
        }
    }
}

struct GreetingScreen_Previews: PreviewProvider {
    static var previews: some View {
        GreetingScreen()
    }
}
