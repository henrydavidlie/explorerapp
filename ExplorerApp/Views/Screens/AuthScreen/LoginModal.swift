//
//  LoginModal.swift
//  ExplorerApp
//
//  Created by Henry David Lie on 06/05/21.
//

import SwiftUI

struct LoginModal: View {
    
    @Environment(\.presentationMode) private var presentationMode
    @State var message: String = ""
    @State private var email: String = ""
    @State private var password: String = ""
    @Binding var showLoginModal: Bool
    @Binding var showGreetModal: Bool
    
    var body: some View {
        NavigationView {
            ZStack {
                Color("Background").ignoresSafeArea()
                VStack {
                    Text("Sign in to your account")
                        .font(.title2)
                        .multilineTextAlignment(.center)
                        .padding(.top)
                        .padding(.bottom)
                    
                    TextField(
                        "Enter email",
                        text: $email)
                        .modifier(TextFieldModifier())
                    
                    SecureField(
                        "Password",
                        text: $password
                    )
                    .modifier(TextFieldModifier())
                    Spacer()
                    Button(action: {
                        ApiService().funcLogin(
                            param: ["email": email, "password": password],
                            completion: { (message) in
                                self.message = message
                                if message == "success" {
                                    self.showGreetModal.toggle()
                                    self.showLoginModal.toggle()
                                } else {
                                    self.showLoginModal.toggle()
                                }
                            })
                    }, label: {
                        Text("Sign in")
                            .foregroundColor(.white)
                    })
                    .modifier(ButtonModifier())
                    .disabled(buttonState())
                    CustomSpacer()
                }
            }
            .navigationBarItems(
                leading:
                    Button("Close") {
                        presentationMode.wrappedValue.dismiss()
                    }.accentColor(Color(UIColor.systemOrange))
            )
            .navigationBarTitleDisplayMode(.inline)
        }
    }
    
    private func buttonState() -> Bool {
        if email.isEmpty || password.isEmpty {
            return true
        } else {
            return false
        }
    }
}
