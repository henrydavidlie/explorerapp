//
//  WelcomeScreen.swift
//  ExplorerApp
//
//  Created by Henry David Lie on 06/05/21.
//

import SwiftUI

struct WelcomeScreen: View {
    
    @Environment(\.colorScheme) var colorScheme
    @State var isGuest = false
    @State var isWithEmail = false
    @State var showSendOTPModal = false
    @State var showLoginModal = false
    @State var showGreetModal = false
    @State private var email: String = ""
    
    var body: some View {
        ZStack {
            Color("Background").ignoresSafeArea()
            VStack {
                Spacer()
                Image("astronaut")
                    .resizable()
                    .scaledToFit()
                    .frame(width: UIScreen.main.bounds.width - 50, height: 400)
                Spacer()
                Button(action: {
                    withAnimation {
                        self.isWithEmail.toggle()
                    }
                }, label: {
                    Text("Continue with Email")
                        .foregroundColor(.white)
                })
                .modifier(ButtonModifier())
                .sheet(isPresented: $isWithEmail) {
                    CheckEmailModal(showSendOTPModal: self.$showSendOTPModal,
                                    isWithEmail: self.$isWithEmail,
                                    showLoginModal: self.$showLoginModal,
                                    mail: self.$email)
                }
                .sheet(isPresented: $showSendOTPModal, content: {
                    SendOTPModal(showSendOTPModal: self.$showSendOTPModal,
                                 showLoginModal: self.$showLoginModal,
                                 email: self.$email)
                })
                .sheet(isPresented: $showLoginModal, content: {
                    LoginModal(showLoginModal: self.$showLoginModal, showGreetModal: self.$showGreetModal)
                })
                .fullScreenCover(isPresented: $showGreetModal, content: {
                    GreetingScreen()
                })
                
                Button(action: {
                    withAnimation {
                        self.isGuest.toggle()
                    }
                }, label: {
                    Text("Continue as guest")
                        .foregroundColor(colorScheme == .dark ? Color.white : Color.black)
                })
                .fullScreenCover(isPresented: $isGuest, content: {
                    ExploreScreen()
                })
                CustomSpacer()
            }
        }
    }
}

struct WelcomeScreen_Previews: PreviewProvider {
    static var previews: some View {
        WelcomeScreen()
    }
}
