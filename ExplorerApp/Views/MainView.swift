//
//  ContentView.swift
//  ExplorerApp
//
//  Created by Henry David Lie on 29/04/21.
//

import SwiftUI

struct MainView: View {
    var body: some View {
        TabView {
            ExploreScreen()
                .tabItem {
                    Label("Explore", systemImage: "bolt.horizontal.fill")
                }
            AccountScreen()
                .tabItem {
                    Label("Account", systemImage: "person.fill")
                }
        }.accentColor(.orange)
    }
}

#if DEBUG
struct MainView_Previews: PreviewProvider {
    static var previews: some View {
        MainView()
    }
}
#endif
