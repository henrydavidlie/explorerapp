//
//  BridgeView.swift
//  ExplorerApp
//
//  Created by Henry David Lie on 07/05/21.
//

import SwiftUI

struct BridgeView: View {
    
    var body: some View {
        if let token = UserDefaults.standard.string(forKey: "AccessToken") {
            print(token)
            return AnyView(MainView())
        } else {
            return AnyView(WelcomeScreen())
        }
    }
}
