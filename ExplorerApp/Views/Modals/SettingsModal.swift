//
//  SettingsModal.swift
//  ExplorerApp
//
//  Created by Henry David Lie on 04/05/21.
//

import SwiftUI

struct SettingsModal: View {
    
    @Environment(\.presentationMode) private var presentationMode
    @Environment(\.colorScheme) var colorScheme
    @State private var showLogoutAlert = false
    @Binding var isShowing: Bool
    @Binding var isLogout: Bool
    @State var message = ""
    
    var body: some View {
        NavigationView {
            ZStack {
                Color("Background").ignoresSafeArea()
                VStack {
                    HStack {
                        Button(action: {}, label: {
                            Text("Change password")
                                .foregroundColor(colorScheme == .dark ? Color.white : Color.black)
                        })
                        Spacer()
                        Image(systemName: "chevron.forward")
                    }
                    .padding()
                    .background(colorScheme == .dark ? Color(UIColor.systemGray5) : Color(UIColor.systemGray6))
                    .cornerRadius(10)
                    .padding()
                    
                    Spacer()
                    Button(action: {
                        showLogoutAlert.toggle()
                    }, label: {
                        Text("Log out")
                            .foregroundColor(Color.white)
                            .frame(width: UIScreen.main.bounds.width - 48, height: 42, alignment: .center)
                    })
                    .background(Color(UIColor.systemRed))
                    .cornerRadius(10)
                    .alert(isPresented: $showLogoutAlert, content: {
                        Alert(
                            title: Text("Log out?"),
                            message: Text("Are you sure want to log out?"),
                            primaryButton: .default(Text("No")),
                            secondaryButton:
                                .destructive(
                                    Text("Yes"),
                                    action: {
                                        ApiService().funcLogout { (message) in
                                            self.message = message
                                            if message == "logged out" {
                                                self.isLogout.toggle()
                                                self.isShowing.toggle()
                                            } else {
                                                self.isShowing.toggle()
                                            }
                                        }
                                    })
                        )
                    })
                    CustomSpacer()
                }
                .navigationTitle("Settings")
                .navigationBarItems(
                    leading:
                        Button(action: {
                            presentationMode.wrappedValue.dismiss()
                        }, label: {
                            Image(systemName: "xmark")
                                .foregroundColor(Color(UIColor.systemOrange))
                        })
                )
                .navigationBarTitleDisplayMode(.inline)
            }
        }
    }
}
