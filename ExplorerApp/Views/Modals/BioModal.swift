//
//  EditBioModal.swift
//  ExplorerApp
//
//  Created by Henry David Lie on 01/05/21.
//

import SwiftUI

struct BioModal: View {
    @Environment(\.presentationMode) private var presentationMode
    
    var body: some View {
        NavigationView {
            VStack {
                Text(/*@START_MENU_TOKEN@*/"Hello, World!"/*@END_MENU_TOKEN@*/)
            }
            .navigationTitle("Edit Bio")
            .navigationBarItems(
                leading:
                    Button("Cancel") {
                        presentationMode.wrappedValue.dismiss()
                    },
                trailing:
                    Button("Done") {
                        presentationMode.wrappedValue.dismiss()
                    }
            )
            .navigationBarTitleDisplayMode(.inline)
        }
        .accentColor(Color(UIColor.systemOrange))
    }
}

#if DEBUG
struct BioModal_Previews: PreviewProvider {
    static var previews: some View {
        BioModal()
    }
}
#endif
