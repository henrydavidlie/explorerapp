//
//  LearnerCardView.swift
//  ExplorerApp
//
//  Created by Henry David Lie on 01/05/21.
//

import SwiftUI

struct LearnerCardView: View {
    
    @State var learner: Explorer
    
    var body: some View {
        HStack {
            ImageLoader(imageUrl: learner.photoURL)
            VStack(alignment: .leading) {
                Text(learner.name)
                Text(learner.expertise)
                    .font(.caption)
            }
            Spacer()
        }
    }
}

#if DEBUG
struct LearnerCardView_Previews: PreviewProvider {
    static var previews: some View {
        LearnerCardView(learner: Explorer.example)
    }
}
#endif
