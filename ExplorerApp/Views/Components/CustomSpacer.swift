//
//  CustomSpacer.swift
//  ExplorerApp
//
//  Created by Henry David Lie on 07/05/21.
//

import SwiftUI

struct CustomSpacer: View {
    var body: some View {
        Rectangle()
            .foregroundColor(Color("Background"))
            .frame(height: 100)
    }
}

struct CustomSpacer_Previews: PreviewProvider {
    static var previews: some View {
        CustomSpacer()
    }
}
